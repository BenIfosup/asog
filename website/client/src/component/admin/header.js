import React from 'react';
import {Button} from "semantic-ui-react";
import {Link} from "react-router-dom";
import Logo from '../Logo/Logo';

const header = (props) => {
    return(
        <header className="headerAdmin">
            <div className="flexbox_horizontale flex_space_between_center">
                <Link className={"logoLink"} to="/">
                    <div>
                        <Logo alternateText={"Gamer un jour, gamer toujours !"}/>
                    </div>
                </Link>
                <Button onClick={() => props.deconnexion()}>Déconnexion</Button>
            </div>
        </header>
    );
};


export default header;
