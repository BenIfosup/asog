import React from 'react'
import {Form, Message, Transition} from "semantic-ui-react";
import {Redirect} from "react-router-dom";

import axios from 'axios';
import {connect} from "react-redux";

class ConnexionForm extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            pseudo: "",
            pass: "",
            showSuccessMessage: false,
            successMessage: "",
            showErrorMessage: false,
            errorMessage: ""
        };
    }

    tryConnexion(){
        const options = {
            method: 'POST',
            headers: {'content-type': 'application/json'},
            data: JSON.stringify({
                query: `query{
                          connexion(pseudo: "${encodeURI(this.state.pseudo)}", mdp: "${encodeURI(this.state.pass)}"){
                            id
                          }
                        }`,
                variables: null
            }),
            url: "/User"
        };
        axios(options).then(val => {
            if(val.data.data.connexion.id){
                this.props.enableConnexion(val.data.data.connexion.id);
            }
            else{
                this.showErrorMessage("Connexion refusée ! Pseudo et/ou mot de passe erroné(s).");
            }
        }, () => {
            this.showErrorMessage("Connexion refusée ! Problème avec le serveur, réessayer plus tard");
        })
    }


    showErrorMessage(message){
        this.setState({showErrorMessage: true, errorMessage: message}, () =>{
            setTimeout(() => {
                this.setState({showErrorMessage: false})
            }, 5000);
        });
    }

    render(){

        let redirect = null;

         if(this.props.connected){
             redirect = <Redirect to={"/admin/addArticle"}/>
         }
        return(
            <Form className={"fullPage flexbox_vertical flex_center_center"}>
                <Form.Field>
                    <label>Pseudo:</label>
                    <Form.Input onChange={(e,d) => this.setState({pseudo: d.value})}/>
                </Form.Field>
                <Form.Field>
                    <label>Mot de passe:</label>
                    <Form.Input type={"password"} onChange={(e,d) => this.setState({pass: d.value})} />
                </Form.Field>
                <Form.Field>
                    <Form.Button onClick={() => this.tryConnexion()}>Envoyer</Form.Button>
                </Form.Field>
                <Transition visible={this.state.showErrorMessage} animation='fade'>
                    <Message
                        error
                        header='Problème !'
                        content={this.state.errorMessage}
                    />
                </Transition>
                {redirect}
            </Form>
    );
    }
}

const mapStateToProps = (state) =>{
    return {
        connected : state.connexion.connected
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        enableConnexion: (id) => {
            dispatch({type: "enableConnexion", payload:{id}});
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ConnexionForm);