import React from 'react';
import axios from 'axios';
import {Image} from "semantic-ui-react";
import {Link} from "react-router-dom";
import Utils from "../utils/utils";

class BigNews extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            articles : [],
            figureClasses: ["first_news_one", "first_news_two", "first_news_three"]
        };
    }

    componentDidMount() {
        this.getArticles();
    }

    getArticles(){
        const options = {
            url: "/article",
            method: "POST",
            headers: {'content-type': 'application/json'},
            data: JSON.stringify({
                query : `query{
                    lastArticles(nbr: 3){
                        title,
                        categorie,
                        header
                    }
                }`,
                variable: {
                    width: 500
                }
            })
        };
        axios(options).then(val => {
            if(val.data.data.lastArticles){
                this.setState({articles: val.data.data.lastArticles.slice(0,3)});
            }
        })
    }

    render(){
        return(
            <section className="first_news">
                {this.state.articles.map((article, index) => {
                    return(
                        <figure key={index} className={this.state.figureClasses[index]}>
                            <Link to={"/article/"+encodeURIComponent(article.title.replace(/ /g, "_"))}>
                                <Image src={article.header+'/'+ Utils.getGoodImageSize()} alt={"Image de couverture"}/>
                                <div className="overlay"/>
                                <figcaption className="flexbox_vertical flex_center">
                                    <p className="categorie">{article.categorie}</p>
                                    <p className="title">{article.title}</p>
                                </figcaption>
                            </Link>
                        </figure>
                    );
                })}
            </section>
        );
    }

}

export default BigNews;