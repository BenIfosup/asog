import React from 'react';
import {Form, Header, Input, Message, Transition} from "semantic-ui-react";
import TextEditor from  './textEditorV2'
import axios from 'axios';
import Hoc from '../container/hoc';
import {connect} from "react-redux";

class articleForm extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            title: "",
            subtitle: "",
            categorie: null,
            initialText: "",
            text: "",
            showSuccessMessage: false,
            successMessage: "",
            showErrorMessage: false,
            errorMessage: "",
            header: null
        };
        if(props.args.id){
            this.state.editMode = true;
            this.state.id = props.args.id;
        }
        else{
            this.state.editMode = false;
        }
    }

    componentDidMount() {
        if(this.state.editMode){
            let promise = this.getArticle(this.state.id);
            promise.then(val => {
                val = val.data.data.articles[0];
                let tmp = {};
                tmp.title = val.title ? val.title : "";
                tmp.subtitle = val.subtitle ? val.subtitle : "";
                tmp.initialText = val.text ? val.text : "";
                tmp.text = val.text ? val.text : "";
                tmp.categorie = val.categorie ? val.categorie : null;
                this.setState(tmp);
            }, () => {
                this.showErrorMessage("L'article n'a pas être chargé suite à une erreur serveur.")
            });
        }
    }

    setTitle(title){
        let tmp = {...this.state};
        tmp.title = title;
        this.setState(tmp);
    }

    setSubtitle(subtitle){
        let tmp = {...this.state};
        tmp.subtitle = subtitle;
        this.setState(tmp);
    }

    setCategorie(cat){
        let tmp = {...this.state};
        tmp.categorie = cat;
        this.setState(tmp);
    }

    setText(text){
        let tmp = {...this.state};
        tmp.text = text;
        this.setState(tmp);
    }


    sendArticle(){
        let formData = new FormData();
        formData.append("file", this.state.header);
        formData.append("query", `mutation{
                          addArticle(title: "${encodeURI(this.state.title)}",
                          subtitle: "${encodeURI(this.state.subtitle)}", 
                          categorie: "${encodeURI(this.state.categorie)}", 
                          text: "${encodeURI(this.state.text)}", 
                          author:"${this.props.id}")
                        }`);

        const options = {
            method: 'POST',
            withCredentials: true,
            headers: {'content-type': 'application/json'},
            data: formData,
            url: "/Article"
        };
        axios(options).then((val) => {
            if(val.data.data.addArticle){
                this.showSuccessMessage(`L'article a été enregistré avec succès !`);
            }
            else{
                this.showErrorMessage(`Le serveur n'a pas pu enregistré l'article ! 
                                   N'oubliez pas que tous les champs sont obligatoires !`);
            }
        }, () => {
            this.showErrorMessage(`Le serveur n'a pas pu enregistré l'article ! 
                                    Sauvegardez celui-ci et réessayez à nouveau. Si le problème persiste, contactez
                                    l'administrateur système.`);
        });
    }

    getArticle(id) {
        const options = {
            method: "POST",
            headers: {'content-type': 'application/json'},
            data: JSON.stringify({
                query: `query{
                          articles(ids: ["${id}"]){
                            title,
                            subtitle,
                            text,
                            categorie
                          }
                        }`,
                variables: null
            }),
            url: "/Article"
        };
        return axios(options);
    }

    editArticle(){
        let formData = new FormData();
        formData.append("file", this.state.header);
        formData.append("query", `mutation{
                          editArticle(
                          id: "${this.state.id}", 
                          categorie: "${encodeURI(this.state.categorie)}", 
                          text: "${encodeURI(this.state.text)}",
                          title: "${encodeURI(this.state.title)}",
                          subtitle: "${encodeURI(this.state.subtitle)}"
                          )
                        }`);
        const options = {
            method: 'POST',
            withCredentials: true,
            headers: {'content-type': 'application/json'},
            data: formData,
            url: "/Article"
        };
        axios(options).then(val => {
            if(val.data.data.editArticle){
                this.showSuccessMessage("L'article a bien été édité !");
            }
            else{
                this.showErrorMessage(`L'article n'a pas pu être modifié ! 
                                    Sauvegardez celui-ci et réessayez à nouveau. Si le problème persiste, contactez
                                    l'administrateur système.`);
            }
        },() => {
            this.showErrorMessage(`L'article n'a pas pu être modifié ! 
                                    Sauvegardez celui-ci et réessayez à nouveau. Si le problème persiste, contactez
                                    l'administrateur système.`);
        });
    }

    showSuccessMessage(message){
        this.setState({showSuccessMessage: true, successMessage: message}, () =>{
            setTimeout(() => {
                this.setState({showSuccessMessage: false})
            }, 3000);
        });
    }

    showErrorMessage(message){
        this.setState({showErrorMessage: true, errorMessage: message}, () =>{
            setTimeout(() => {
                this.setState({showErrorMessage: false})
            }, 5000);
        });
    }


    render() {
        return(
            <Hoc>
                <Header as='h1'>Ajouter un article</Header>
                <Form>
                    <Form.Field>
                        <label>Titre</label>
                        <Input value={this.state.title} onChange={(e, d ) => this.setTitle(d.value)} placeholder="Titre de l'article"/>
                    </Form.Field>
                    <Form.Field>
                        <label>Sous-titre</label>
                        <Input value={this.state.subtitle} onChange={(e, d ) => this.setSubtitle(d.value)} placeholder="Sous-titre de l'article"/>
                    </Form.Field>
                    <Form.Field>
                        <label>Catégorie</label>
                        {
                            this.state.categorie ?
                                (
                                    <Form.Select
                                        value={this.state.categorie}
                                        onChange={(e, d) => this.setCategorie(d.value)}
                                        placeholder='Choisissez une catégorie'
                                        options={this.props.args.cats}/>
                                ):
                                (
                                    <Form.Select
                                        onChange={(e, d) => this.setCategorie(d.value)}
                                        placeholder='Choisissez une catégorie'
                                        options={this.props.args.cats}/>
                                )
                        }
                    </Form.Field>
                    <Form.Field>
                        <label>Header:</label>
                        <Form.Input type={"file"} accept="image/*" onChange={(e)=>{
                            this.setState({header: e.target.files[0]});
                        }} />
                    </Form.Field>
                    <TextEditor data={this.state.initialText} onChange={(text) => this.setText(text)}/>
                    <Form.Field>
                        <Form.Button onClick={()=>{
                            if(this.state.editMode){
                                this.editArticle();
                            }
                            else{
                                this.sendArticle();
                            }
                        }}> Envoyer </Form.Button>
                    </Form.Field>
                </Form>
                <Transition visible={this.state.showSuccessMessage} animation='fade'>
                    <Message
                        success
                        header='Ok !'
                        content={this.state.successMessage}
                    />
                </Transition>
                <Transition visible={this.state.showErrorMessage} animation='fade'>
                    <Message
                        error
                        header='Problème !'
                        content={this.state.errorMessage}
                    />
                </Transition>
            </Hoc>
        )
    }
}

const mapStateToProps = (state) =>{
    return {
        id : state.connexion.id
    }
};

export default connect(mapStateToProps)(articleForm);
