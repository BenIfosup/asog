import React from 'react';
import {Divider} from "semantic-ui-react";
import axios from "axios";
import HOC from '../container/hoc';
import MetaTags from "react-meta-tags";

class Page extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            title: props.args.title.replace(/_/g, " "),
            text: ""
        };
    }

    componentDidMount() {
        this.loadPage();
    }

    componentDidUpdate(prevProps) {
        if(this.props !== prevProps){
            this.setState({title: this.props.args.title.replace(/_/g, " "), text: ""}, () => {
                this.loadPage();
            });
        }
    }

    loadPage(){
        const options = {
            method: 'POST',
            headers: {'content-type': 'application/json'},
            url: "/page",
            data: JSON.stringify({
                query: `query{
                    page(title: "${encodeURI(this.state.title)}"){
                        text
                    }
                }`
            })
        };
        axios(options).then(val => {
            if(val.data.data.page.text){
                this.setState({text: val.data.data.page.text});
            }
        });
    }

    render(){
        return(
            <article key={this.state.title}>
                <MetaTags>
                    <title>{this.state.title} – All Styles of Gaming</title>
                    <meta name="description" content={"Page " + this.state.title + " du site All Styles Of Gaming"} />
                    <meta name="title" content={this.state.title + " – All Styles of Gaming"} />
                </MetaTags>
                <h1 className="title">{this.state.title}</h1>
                <Divider/>
                <HOC>
                    <div dangerouslySetInnerHTML={{__html: this.state.text}} />
                </HOC>
            </article>
        );
    }
}

export default Page