import React from 'react';
import ArticleForm from './articleForm';
import SearchAdmin from './searchAdmin';
import PageEditor from './pageEditor';

const rightPanelAdmin = (props) => {

    let Component = null;
    switch (props.right){
        case 'ArticleForm':
            Component = ArticleForm;
            break;
        case 'SearchAdmin':
            Component = SearchAdmin;
            break;
        case "PageEditor":
            Component = PageEditor;
            break;
        default:
            Component = ArticleForm;
            break;
    }

    return (
        <div className="panel_container_right">
            <Component args={props.propsComponent}/>
        </div>
    );
};

export default rightPanelAdmin;