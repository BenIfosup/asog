import React from 'react';
import axios from 'axios';
import {Table} from "semantic-ui-react";
import moment from 'moment';
import {Link} from "react-router-dom";

class TableNews extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            articles: []
        }
    }

    componentDidMount() {
        this.getArticles();
    }

    getArticles(){
        const options = {
            url: "/article",
            method: "POST",
            headers: {'content-type': 'application/json'},
            data: JSON.stringify({
                query : `query{
                    lastArticles(nbr: 18){
                        id,
                        title,
                        categorie,
                        date
                    }
                }`,
                variable: null
            })
        };
        axios(options).then(val => {
            if(val.data.data.lastArticles){
                this.setState({articles: val.data.data.lastArticles.slice(3)});
            }
        })
    }

    render(){
        return(
            <section className="list_news">
                <h2 className="header_flux">Dernières actualités du site:</h2>
                <Table>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell width={3}>Date</Table.HeaderCell>
                            <Table.HeaderCell width={3}>Categorie</Table.HeaderCell>
                            <Table.HeaderCell>Titre</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        {this.state.articles.map((article, index) => {
                            return(
                                <Table.Row key={index}>
                                    <Table.Cell textAlign="center">{moment(article.date).format('DD/MM/YYYY')}</Table.Cell>
                                    <Table.Cell textAlign="center">{article.categorie}</Table.Cell>
                                    <Table.Cell>
                                        <Link to={"/article/"+encodeURIComponent(article.title.replace(/ /g, "_"))} className="grey_visited">
                                            <strong>{article.title}</strong>
                                        </Link>
                                    </Table.Cell>
                                </Table.Row>
                            );
                        })}
                    </Table.Body>
                </Table>
            </section>
        );
    }
}

export default TableNews;