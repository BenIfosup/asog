import React from 'react';
import LeftPanelAdmin from './leftPanelAdmin';
import RightPanelAdmin from './rightPanelAdmin';

const panelAdmin = props => {

    let cats = [
        {value: "News", text: "News"},
        {value: "Dossier", text: "Dossier"},
        {value: "Test", text: "Test"},
        {value: "Divers", text: "Divers"}
    ];


    let propsComponent = null;
    switch (props.right) {
        case "ArticleForm":
            propsComponent = {};
            propsComponent.cats = cats;
            if(props.match && props.match.params && props.match.params.id){
                propsComponent.id = props.match.params.id;
            }
            break;
        case "SearchAdmin":
            if(props.match && props.match.params && props.match.params.categorie){
                propsComponent = props.match.params.categorie;
            }
            else{
                propsComponent = null;
            }
            break;
        case "PageEditor":
            if(props.match && props.match.params && props.match.params.title){
                propsComponent = props.match.params.title.replace(/_/g, " ");
            }
            else{
                propsComponent = null;
            }
            break;
        default:
            propsComponent = null;
            break;
    }


    return (
        <div className="flexbox_horizontale panel_admin">
            <LeftPanelAdmin/>
            <RightPanelAdmin right={props.right} propsComponent={propsComponent}/>
        </div>
    );
};

export default panelAdmin;