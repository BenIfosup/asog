import React from 'react';
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-editor-classic/src/classiceditor'

import Essentials from '@ckeditor/ckeditor5-essentials/src/essentials';
import Bold from '@ckeditor/ckeditor5-basic-styles/src/bold';
import Italic from '@ckeditor/ckeditor5-basic-styles/src/italic';
import Paragraph from '@ckeditor/ckeditor5-paragraph/src/paragraph';
import Heading from '@ckeditor/ckeditor5-heading/src/heading';
import Code from '@ckeditor/ckeditor5-basic-styles/src/code';
import Strikethrough from '@ckeditor/ckeditor5-basic-styles/src/strikethrough';
import Subscript from '@ckeditor/ckeditor5-basic-styles/src/subscript';
import Superscript from '@ckeditor/ckeditor5-basic-styles/src/superscript';
import Underline from '@ckeditor/ckeditor5-basic-styles/src/underline';
import Font from '@ckeditor/ckeditor5-font/src/font';
import Highlight from '@ckeditor/ckeditor5-highlight/src/highlight';
import Image from '@ckeditor/ckeditor5-image/src/image';
import ImageToolbar from '@ckeditor/ckeditor5-image/src/imagetoolbar';
import ImageCaption from '@ckeditor/ckeditor5-image/src/imagecaption';
import ImageStyle from '@ckeditor/ckeditor5-image/src/imagestyle';
import Link from '@ckeditor/ckeditor5-link/src/link';
import MediaEmbed from '@ckeditor/ckeditor5-media-embed/src/mediaembed';
import PasteFromOffice from '@ckeditor/ckeditor5-paste-from-office/src/pastefromoffice';
import RemoveFormat from '@ckeditor/ckeditor5-remove-format/src/removeformat';
import Table from '@ckeditor/ckeditor5-table/src/table';
import TableToolbar from '@ckeditor/ckeditor5-table/src/tabletoolbar';
import Alignment from '@ckeditor/ckeditor5-alignment/src/alignment';
import List from '@ckeditor/ckeditor5-list/src/list';
import CKFinder from '@ckeditor/ckeditor5-ckfinder/src/ckfinder';

const editorConfiguration = {
    plugins: [
        Essentials,
        Bold,
        Italic,
        Strikethrough,
        Paragraph,
        Heading,
        Code,
        Subscript,
        Superscript,
        Underline,
        Font,
        Highlight,
        Image,
        ImageToolbar,
        ImageCaption,
        ImageStyle,
        Link,
        MediaEmbed,
        PasteFromOffice,
        RemoveFormat,
        Table,
        TableToolbar,
        Alignment,
        List,
        CKFinder
    ],
    image: {
        toolbar: [
            'imageTextAlternative',
            '|',
            'imageStyle:full',
            'imageStyle:alignLeft',
            'imageStyle:alignCenter',
            'imageStyle:alignRight'
        ],
        styles: [
            // This option is equal to a situation where no style is applied.
            'full',

            // This represents an image aligned to the left.
            'alignLeft',

            // This represents an image aligned to the right.
            'alignRight',
            'alignCenter'
        ]
    },
    table: {
        contentToolbar: [ 'tableColumn', 'tableRow', 'mergeTableCells' ]
    },
    ckfinder: {
        // Upload the images to the server using the CKFinder QuickUpload command.
        uploadUrl: 'http:localhost:8080/',

        // Define the CKFinder configuration (if necessary).
        options: {
            resourceType: 'Images'
        }
    },
    toolbar: [
        'ckfinder',
        'undo',
        'redo',
        '|',
        'heading',
        '|',
        'fontSize',
        'fontFamily',
        'fontColor',
        'fontBackgroundColor',
        'bold',
        'italic',
        'strikethrough',
        'code',
        'subscript',
        'superscript',
        'underline',
        'Highlight',
        'link',
        'mediaEmbed',
        'removeFormat',
        'insertTable',
        'alignment',
        'bulletedList',
        'numberedList'
    ]
};


const textEditor = (props) => {
    return(
        <CKEditor
            editor={ ClassicEditor }
            config={ editorConfiguration }
            data={props.data}
            onInit={ editor => {
                // You can store the "editor" and use when it is needed.
                //console.log( 'Editor is ready to use!', editor );
            } }
            onChange={ ( event, editor ) => {
                const data = editor.getData();
                props.onChange(data);
                //console.log( { event, editor, data } );
            } }
            onBlur={ editor => {
                //console.log( 'Blur.', editor );
            } }
            onFocus={ editor => {
                //console.log( 'Focus.', editor );
            } }
        />
    );
};

export default textEditor;