import React from 'react'
import {Table, Pagination} from "semantic-ui-react";
import moment from 'moment';
import axios from 'axios';
import {Link} from "react-router-dom";
import MetaTags from "react-meta-tags";

class Search extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            categorie: props.args.categorie,
            articles: [],
            articlesToShow: [],
            nbrElemToShow: 10,
            activeIndex: 0,
            searchField: props.args.searchField
        }
    }

    componentDidMount() {
        this.loadResult();
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.args !== prevProps.args) {
            this.setState({categorie: this.props.args.categorie, searchField: this.props.args.searchField},
                () =>{
                this.loadResult();
            })

        }
    }

    changeActifIndex(index){
        this.setState({
            activeIndex: index,
            articlesToShow: this.state.articles.splice(
                (index)*this.state.nbrElemToShow,
                (index+1)*this.state.nbrElemToShow
            )
        })
    }

    loadResult(){
        let query = "";
        if(this.state.categorie === "General"){
            query = `query{
                results: filteredArticlesByString(string: "${encodeURI(this.state.searchField)}"){
                    id,
                    title,
                    categorie,
                    date
                }
            }`
        }
        else{
            query = `query{
                results: filteredArticles(categorie: "${encodeURI(this.props.args.categorie)}"){
                    id,
                    title,
                    date
                }
            }`
        }

        const options = {
            url: "/article",
            method: "POST",
            headers: {'content-type': 'application/json'},
            data: JSON.stringify({
                query,
                variable: null
            })
        };

        axios(options).then(val => {
            if(val.data.data.results){
                const articles = val.data.data.results;
                const articlesToShow = articles.slice(0, this.state.nbrElemToShow);
                this.setState({articles, articlesToShow});
            }
        });
    }

    render() {
        return (
            <section className="search_result">
                {
                    this.state.categorie === "General" ?
                        (
                            <MetaTags>
                                <title>Recherche – All Styles of Gaming</title>
                                <meta name="description" content={"Résultat de la recherche sur le site All Styles of Gaming"} />
                                <meta name="title" content={"Recherche - All Styles of Gaming"} />
                            </MetaTags>
                        )
                        :
                        (
                            <MetaTags>
                                <title>{this.state.categorie} – All Styles of Gaming</title>
                                <meta name="description" content={this.state.categorie + " du site All Styles of Gaming"} />
                                <meta name="title" content={this.state.categorie + " du site All Styles of Gaming"} />
                            </MetaTags>
                        )
                }
                <h2 className="header_result">{this.props.args.searchName}</h2>
                <Table striped celled>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell width={3}>Date</Table.HeaderCell>
                            {
                                this.state.categorie === "General" ?
                                    (<Table.HeaderCell width={3}>Categorie</Table.HeaderCell>) : null
                            }
                            <Table.HeaderCell>Titre</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        {this.state.articles.map((r, i) => {
                            return(
                                <Table.Row key={i}>
                                    <Table.Cell textAlign={"center"}>{moment(r.date).format("DD/MM/YYYY")}</Table.Cell>
                                    {
                                        this.state.categorie === "General" ?
                                            (<Table.Cell textAlign={"center"}>{r.categorie}</Table.Cell>) : null
                                    }
                                    <Table.Cell>
                                        <Link className="grey_visited" to={"/article/" + encodeURIComponent(r.title.replace(/ /g,'_'))}>
                                            <strong>{r.title}</strong>
                                        </Link>
                                    </Table.Cell>
                                </Table.Row>
                            );
                        })}
                    </Table.Body>
                </Table>
                <Pagination
                    totalPages={Math.ceil(this.state.articles.length / this.state.nbrElemToShow)}
                    activePage={this.state.activeIndex+1}
                    onPageChange={(e,d)=>this.changeActifIndex(d.activePage-1)}
                />
            </section>
        );
    }
}

export default Search;