import React from 'react';
import {Form, Table, Modal, Transition, Message} from "semantic-ui-react";
import {Link} from "react-router-dom";
import Button from "semantic-ui-react/dist/commonjs/elements/Button";

import HOC from '../container/hoc';
import Pagination from "semantic-ui-react/dist/commonjs/addons/Pagination";
import axios from 'axios';
import moment from 'moment'

class searchAdmin extends React.Component{

    constructor(props){
        super(props);
        let articles = [];
        let articlesFiltered = [];
        let activeIndex = 0;
        const numberElem = 4;
        let articlesToShow = [];
        let maxNumberPage = 0;
        let openModal = false;
        let titleArticleToDelete= null;
        let idArticleToDelete = null;
        let showSuccessMessage = false;
        let showErrorMessage = false;
        this.state = {
            articles,
            articlesFiltered,
            activeIndex,
            numberElem,
            articlesToShow,
            maxNumberPage,
            openModal,
            titleArticleToDelete,
            idArticleToDelete,
            showErrorMessage,
            showSuccessMessage
        };
    }

    componentDidMount() {
        let articleP = this.loadArticles(this.props.args);
        articleP.then(articles => {
            let articlesFiltered = articles;
            let activeIndex = 0;
            let articlesToShow = articles.slice(activeIndex*this.state.numberElem, this.state.numberElem * (activeIndex+1));
            let maxNumberPage = Math.ceil(articlesFiltered.length/this.state.numberElem);
            this.setState({
                articles,
                articlesFiltered,
                activeIndex,
                articlesToShow,
                maxNumberPage
            });
        }, () => {
            this.showErrorMessage("Les articles n'ont pas pu être chargés suite à un problème serveur.");
        });
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(this.props.args !== prevProps.args){
            let articleP = this.loadArticles(this.props.args);
            articleP.then(articles => {
                let articlesFiltered = articles;
                let activeIndex = 0;
                let articlesToShow = articles.slice(activeIndex*this.state.numberElem, this.state.numberElem * (activeIndex+1));
                let maxNumberPage = Math.ceil(articlesFiltered.length/this.state.numberElem);
                this.setState({
                    articles,
                    articlesFiltered,
                    activeIndex,
                    articlesToShow,
                    maxNumberPage
                });
            }, () => {
                this.showErrorMessage("Les articles n'ont pas pu être chargés suite à un problème serveur.");
            });
        }
    }

    updateArticleToShow(){
        let tmp = {...this.state};
        tmp.articlesToShow = tmp.articlesFiltered.slice(tmp.activeIndex*tmp.numberElem, tmp.numberElem * (tmp.activeIndex+1));
        this.setState(tmp);
    }

    changeActifIndex(index) {
        let tmp = {...this.state};
        tmp.activeIndex = index;
        this.setState(tmp, () => this.updateArticleToShow());
    }

    filterArticles(string){
        let tmp = {...this.state};
        tmp.articlesFiltered = tmp.articles.filter(article => {
            return article.date.includes(string) || article.title.toLowerCase().includes(string.toLowerCase());
        });
        tmp.maxNumberPage = Math.ceil(tmp.articlesFiltered.length / tmp.numberElem);
        tmp.activeIndex = 0;
        tmp.articlesToShow = tmp.articlesFiltered.slice(tmp.activeIndex*tmp.numberElem, tmp.numberElem * (tmp.activeIndex+1));
        this.setState(tmp);
    }

    loadArticles(categorie){
        const options = {
            url: "/article",
            method: "POST",
            headers: {'content-type': 'application/json'},
            data: JSON.stringify({
                query:
                    `query{
                        filteredArticles(categorie: "${encodeURI(categorie)}"){
                            id,
                            title,
                            date
                        }
                    }`
                ,
                variable: null
            })
        };
        return new Promise((resolve, reject) => {
            axios(options).then(value => {
                resolve(value.data.data.filteredArticles)
                }, err  => {
                    reject(err);
                }
            );
        });
    }

    deleteArticle(){
        const options = {
            url: "/article",
            method: "POST",
            withCredentials: true,
            headers: {'content-type': 'application/json'},
            data: JSON.stringify({
                query:
                    `mutation{
                        deleteArticle(id: "${this.state.idArticleToDelete}")
                    }`
                ,
                variable: null
            })
        };
        axios(options).then(() => {
            let {articles, articlesFiltered} = this.state;
            articles = articles.filter(article => this.state.idArticleToDelete !== article.id);
            articlesFiltered = articlesFiltered.filter(article => this.state.idArticleToDelete !== article.id);
            let articlesToShow = articlesFiltered.slice(this.state.activeIndex*this.state.numberElem, (this.state.activeIndex+1)*this.state.numberElem);
            this.setState({openModal: false, articlesToShow, articles, articlesFiltered}, () => {
                this.showSuccessMessage("L'article a été supprimé avec succès !");
            })
        }, () => {
            this.setState({openModal: false}, () => {
                this.showErrorMessage("L'article n'a pas pu être supprimé ! Veuillez réessayer plus tard.");
            });
        });
    }

    showSuccessMessage(message){
        this.setState({showSuccessMessage: true, successMessage: message}, () =>{
            setTimeout(() => {
                this.setState({showSuccessMessage: false})
            }, 3000);
        });
    }

    showErrorMessage(message){
        this.setState({showErrorMessage: true, errorMessage: message}, () =>{
            setTimeout(() => {
                this.setState({showErrorMessage: false})
            }, 5000);
        });
    }

    render(){
        return (
            <HOC key={this.props.args}>
                <Form>
                    <Form.Field>
                        <label>Recherche {this.props.args.toLowerCase()}</label>
                        <Form.Input onChange={(e, d) => this.filterArticles(d.value)} icon='search'/>
                        <Table striped celled>
                            <Table.Header>
                                <Table.Row textAlign="center">
                                    <Table.HeaderCell width={1}>Date</Table.HeaderCell>
                                    <Table.HeaderCell width={13}>Titre</Table.HeaderCell>
                                    <Table.HeaderCell width={2}>Gestion</Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>
                            <Transition.Group as={Table.Body} animation="fade">
                                {this.state.articlesToShow.map((d) => {
                                    return(
                                        <Table.Row key={d.id}>
                                            <Table.Cell>{moment(d.date).format("DD/MM/YYYY")}</Table.Cell>
                                            <Table.Cell>{d.title}</Table.Cell>
                                            <Table.Cell>
                                                <Link to={"/admin/edit/"+d.id}><Button fluid content="Editer"/></Link>
                                                <Button fluid onClick={() => {
                                                    this.setState({
                                                        openModal: true,
                                                        titleArticleToDelete: d.title,
                                                        idArticleToDelete: d.id
                                                    })
                                                }}>Supprimer</Button>
                                            </Table.Cell>
                                        </Table.Row>
                                    )
                                })}
                            </Transition.Group>
                        </Table>
                    </Form.Field>
                </Form>
                <Modal
                    open={this.state.openModal}
                >
                    <Modal.Header>Attention ! </Modal.Header>
                    <Modal.Content>
                        <p>Êtes-vous certains de vouloir supprimer l'article "{this.state.titleArticleToDelete}" ?</p>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button color="green" onClick={()=>this.setState({openModal: false})}>Garder l'article</Button>
                        <Button color="red" onClick={() => this.deleteArticle()}>Supprimer l'article</Button>
                    </Modal.Actions>
                </Modal>
                <Transition visible={this.state.showSuccessMessage} animation='fade'>
                    <Message
                        success
                        header='Ok !'
                        content={this.state.successMessage}
                    />
                </Transition>
                <Transition visible={this.state.showErrorMessage} animation='fade'>
                    <Message
                        error
                        header='Problème !'
                        content={this.state.errorMessage}
                    />
                </Transition>
                <p>Aller à la page:</p>
                <Pagination
                    totalPages={this.state.maxNumberPage}
                    activePage={this.state.activeIndex+1}
                    onPageChange={(e,d)=>this.changeActifIndex(d.activePage-1)}/>
            </HOC>
        );
    }
}

export default searchAdmin;