import React from 'react';
import {Menu, Dropdown} from "semantic-ui-react";
import {Link} from "react-router-dom";

const leftPanelAdmin = () => {
    return (
        <Menu vertical as="nav">
            <Link to="/admin/addArticle">
                <Menu.Item>
                    <div className={"ui item dropdown"}>Ajouter un articler</div>
                </Menu.Item>
            </Link>
            <Menu.Item>
                <Dropdown item text="Gestion articles" >
                    <Dropdown.Menu>
                        <Link to="/admin/gestion/News"><Dropdown.Item>News</Dropdown.Item></Link>
                        <Link to="/admin/gestion/Dossier"><Dropdown.Item>Dossier</Dropdown.Item></Link>
                        <Link to="/admin/gestion/Test"><Dropdown.Item>Test</Dropdown.Item></Link>
                        <Link to="/admin/gestion/Divers"><Dropdown.Item>Divers</Dropdown.Item></Link>
                    </Dropdown.Menu>
                </Dropdown>
            </Menu.Item>
            <Menu.Item>
                <Dropdown item text="Gestion pages">
                    <Dropdown.Menu>
                        <Link to="/admin/gestion/pages/A_propos"><Dropdown.Item>A propos</Dropdown.Item></Link>
                        <Link to="/admin/gestion/pages/Mentions_légales"><Dropdown.Item>Mentions légales</Dropdown.Item></Link>
                        <Link to="/admin/gestion/pages/Mentions_licences"><Dropdown.Item>Mentions licences</Dropdown.Item></Link>
                    </Dropdown.Menu>
                </Dropdown>
            </Menu.Item>
        </Menu>
    )
};

export default leftPanelAdmin;