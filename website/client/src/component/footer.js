import React from 'react';
import {Icon} from "semantic-ui-react";
import {Link} from "react-router-dom";

const footer = () => {
    return(
        <footer>
            <div className="footer flexbox_horizontale flex_space_between">
                <div className="networks flexbox_vertical flex_center_center">
                    <p>Suivez nous sur:</p>
                    <p>
                        <a
                            href={"https://www.facebook.com/All-Styles-Of-Gaming-1506998159513470/"}
                            target={"_blank"}
                            aria-label={"Page Facebook du site"}
                            rel="noopener"
                        >
                            <Icon name={"facebook square"} size="big"/>
                        </a>
                        <a
                            href={"https://twitter.com/AllStylesOfGami"}
                            target={"_blank"}
                            aria-label={"Page Twitter du site"}
                            rel="noopener"
                        >
                            <Icon name={"twitter square"} size="big"/>
                        </a>
                        <a href={"/rss"} target={"_blank"} aria-label={"Flux RSS du site"} ><Icon name={"rss square"} size="big"/></a>
                    </p>
                </div>
                <div className="licences flexbox_vertical flex_space_between_center">
                    <Link to={"/page/Mentions_légales"} className={"white"}><p>Mentions légales</p></Link>
                    <Link to={"/page/Mentions_licences"} className={"white"}><p>Mentions licences</p></Link>
                </div>
                <div className="contact flexbox_vertical flex_center_center">
                    <Link to={"/contact"} className={"white"}><p>Contactez-nous</p></Link>
                </div>
            </div>
        </footer>
    );
};

export default footer;