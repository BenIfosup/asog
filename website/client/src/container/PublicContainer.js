import React, {Suspense} from 'react';

const Home = React.lazy(() => import('../component/home'));
const Article = React.lazy(() => import("../component/article"));
const Search = React.lazy(() => import('../component/search'));
const Page = React.lazy(() => import('../component/page'));
const Contact = React.lazy(() => import('../component/contactForm'));

const publicContainer = props => {

    let Content = null;
    let args = {};

    switch (props.content) {
        case "home":
            Content = Home;
            break;
        case "article":
            Content = Article;
            if(props.match && props.match.params && props.match.params.title){
                args.title = props.match.params.title;
            }
            break;
        case "page":
            Content = Page;
            if(props.match && props.match.params && props.match.params.title){
                args.title = props.match.params.title;
            }
            break;
        case "search":
            Content = Search;
            if(props.match && props.match.params && props.match.params.categorie){
                args.categorie = props.match.params.categorie;
                switch (args.categorie) {
                    case "News":
                        args.searchName = "Liste des news du site:";
                        break;
                    case "Test":
                        args.searchName = "Liste des tests du site:";
                        break;
                    case "Dossier":
                        args.searchName = "Liste des dossiers du site:";
                        break;
                    case "Divers":
                        args.searchName = "Liste des articles divers du site:";
                        break;
                    default:
                        args.categorie = "General";
                        args.searchName = "Résultat de votre recherche:";
                        args.searchField = "";
                }
            }
            else{
                args.categorie = "General";
                args.searchName = "Résultat de votre recherche:";
                if(props.match && props.match.params && props.match.params.searchField){
                    args.searchField = props.match.params.searchField;
                }
                else{
                    args.searchField = "";
                }
            }
            break;
        case 'contact':
            Content = Contact;
            break;
        default:
            Content = Home;
    }

    return(
        <Suspense fallback={<div>Loading...</div>}>
            <main className="ui container">
                <Content args={args}/>
            </main>
        </Suspense>
    );
};



export default publicContainer;