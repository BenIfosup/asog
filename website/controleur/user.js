const userDB = require('../model/DB/db').User;
const graphql = require('graphql');
const sanitize = require('mongo-sanitize');
const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLInt,
    GraphQLList,
    GraphQLSchema,
    GraphQLNonNull,
    GraphQLBoolean
} = graphql;


const UserType = new GraphQLObjectType({
    name: "User",
    fields: () => ({
        id: {type: GraphQLString},
        pseudo: {type: GraphQLString}
    })
});

const RootQuery =  new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        connexion: {
            type: UserType,
            args: {
                pseudo: {type: new GraphQLNonNull(GraphQLString)},
                mdp: {type: new GraphQLNonNull(GraphQLString)}
            },
            resolve(parent, args, request){
                args = sanitize(args);
                Object.keys(args).map(k => {
                    args[k] = decodeURI(args[k]);
                });
                return new Promise((resolve, reject) => {
                    userDB.findOne({pseudo: args.pseudo}).then(user => {
                        if(user && user.compareMDP(args.mdp)){
                            request.session.connected = true;
                            request.session.userId = user.id;
                            resolve(user);
                        }
                        resolve(false);
                    }, err => {
                        console.log(err);
                        reject(false);
                    });
                });
            }
        },
        isAlreadyConnected:{
            type: UserType,
            resolve(parent, args, request){
                return new Promise((resolve, reject) => {
                   if(request.session.connected){
                       userDB.findById(request.session.userId).then(user => {
                           resolve(user);
                       }, err => {
                           console.log(err);
                           reject(err);
                       });
                   }
                   else{
                       reject("You're not connected");
                   }
                });
            }
        },
        deconnexion:{
            type: GraphQLBoolean,
            resolve(parent, args, request){
                if(request.session.connected){
                    request.session.connected = null;
                }
                if(request.session.userId){
                    request.session.userId = null;
                }
                request.session.destroy();
                return true;
            }
        }
    }
});

module.exports = new GraphQLSchema({
    query: RootQuery
    //mutation: MutationQuery
});