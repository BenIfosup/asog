const imageManager = require('../model/imagesManager');

module.exports.image = function(req, res){
    let width = req.params.width;
    if(isNaN(width)){
        width = 1920;
    }
    else{
        width = parseInt(width);
    }
    let wepbEnable = req.get("Accept").includes("webp");
    let filename = req.params.filename;
    if(wepbEnable){
        filename += '.webp';
    }

    let promise = imageManager.getImage(filename, width);
    promise.then((stream) => {
        stream.pipe(res);
        res.on('close', function() {
            res.end();
        });
    }, () => {
        res.sendStatus(404);
    });
};