const mail = require('../model/mail');

module.exports.handleRequestContact = (req, res) => {
    if(req.body){
        let checkEmail = req.body.email && req.body.email.includes('@') && req.body.email.includes('.');
        let checkSubject = req.body.subject && (req.body.subject !== "");
        let checkContent = req.body.content && (req.body.content !== "");
        if(checkEmail && checkSubject && checkContent){
            mail.sendMail(req.body.email, req.body.subject, req.body.content).then(() => {
                res.sendStatus(200);
            }, err => {
                console.log(err);
                res.sendStatus(500);
            })
        }
        else{
            //error in one or more field
            res.sendStatus(400);
        }
    }
    else{
        // no body --> error
        res.sendStatus(400);
    }
};