const RSS  = require('rss');
const articleDB = require('../model/DB/article');
const userDB = require('../model/DB/user');

module.exports.generateRSS = (req, res) => {
    let promise = new Promise((resolve, reject) => {
        articleDB
            .find()
            .limit(20)
            .exec()
            .then(articles => {
                let articlesWithAuthor = articles.map(article => {
                    return userDB.findById(article.author).exec().then(author => {
                        return {article, author};
                    }, err => {
                        console.log(err);
                        reject(err);
                    })
                });
                Promise.all(articlesWithAuthor).then(vals => {
                    resolve(vals);
                }, (err) => {
                    reject(err);
                });
            }, err => {
                console.log(err);
                reject(err);
            })
    });

    let myFeed = new RSS({
        title: "Flux RSS de All Styles Of Gaming",
        feed_url: "http://allstylesofgaming.com/rss",
        site_url: "http://allstylesofgaming.com/",
        language: "fr", // optional, used only in RSS 2.0, possible values: http://www.w3.org/TR/REC-html40/struct/dirlang.html#langcodes
        feedLinks: {
            json: "http://allstylesofgaming.com/rss",
        }
    });

    promise.then(articlesWithAuthor => {
        articlesWithAuthor.map(articleWithAuthor => {
            myFeed.item({
                title: articleWithAuthor.article.title,
                guid: articleWithAuthor.article.id,
                url: "http://allstylesofgaming.com/article/" + articleWithAuthor.article.title.replace(/ /g, "_"),
                description: articleWithAuthor.article.text,
                author: [
                    {
                        name: articleWithAuthor.author.pseudo
                    }
                ],
                date: articleWithAuthor.article.date
            });
        });
        res.format({
            "application/xml": () => {
                res.send(myFeed.xml());
            }
        });

    }, () => {
        res.sendStatus(500);
    });
};