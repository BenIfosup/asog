const pageDB = require('../model/DB/db').Page;
const sanitize = require('mongo-sanitize');
const graphql = require('graphql');
const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLInt,
    GraphQLList,
    GraphQLSchema,
    GraphQLNonNull,
    GraphQLBoolean
} = graphql;

const PageType = new GraphQLObjectType({
    name: "Page",
    fields: () => ({
        id: {type: GraphQLString},
        text: {type: GraphQLString}
    })
});

const MutationQuery = new GraphQLObjectType({
    name: "Mutation",
    fields : () => ({
        editPage: {
            type: GraphQLBoolean,
            args: {
                id: {type: GraphQLString},
                title: {type: GraphQLString},
                text: {type: GraphQLString}
            },
            resolve(parent, args, request){
                args = sanitize(args);
                Object.keys(args).map(k => {
                    args[k] = decodeURI(args[k]);
                });
                return new Promise((resolve, reject) => {
                    if(!request.session.connected){
                        return reject("You must be connected");
                    }
                    if(args.id){
                        pageDB.findByIdAndUpdate(args.id, args).exec().then(
                            () => resolve(true),
                            err => {
                                console.log(err);
                                reject(false);
                            })
                    }
                    else if(args.title){
                        pageDB.findOneAndUpdate({title: args.title}, args).exec().then(
                            () => resolve(true),
                            err => {
                                console.log(err);
                                reject(false);
                            }
                        );

                    }
                    else{
                        reject("id or name field must be provided");
                    }
                });
            }
        }
    })
});

const QueryRoot = new GraphQLObjectType({
    name: "QueryRoot",
    fields: () => ({
        page: {
            type: PageType,
            args: {
                title: {type: new GraphQLNonNull(GraphQLString)}
            },
            resolve(parent, args) {
                args = sanitize(args);
                args.title = decodeURI(args.title);
                return pageDB.findOne({title: args.title}).exec();
            }
        }
    })
});


module.exports = new GraphQLSchema({
    query: QueryRoot,
    mutation: MutationQuery
});