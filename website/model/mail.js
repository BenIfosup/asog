const nodemailer = require('nodemailer');
const emailTeam = "equipe@allstylesofgaming.com";

let transporter = nodemailer.createTransport({
    debug: true,
    logger: true,
    sendmail: true
});

module.exports.sendMail = async function(email, subject, content){
    return await transporter.sendMail({
        from: email,
        to: emailTeam,
        subject,
        text: content
    });
};