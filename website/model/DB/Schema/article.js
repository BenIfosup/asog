const mongoose = require('../mongoose');
const schema = new mongoose.Schema({
    title: {type: String, required: true, unique: true},
    subtitle: {type: String, required: true},
    categorie: {type: String, required: true},
    text: {type: String, required: true},
    date: {type: Date, default: Date.now()},
    header: {type: String, required: true},
    author: {type: mongoose.Types.ObjectId, required: true}
});
module.exports.schema = schema;