module.exports.config = require("./config/db.config");
module.exports.User = require("./user");
module.exports.Article = require("./article");
module.exports.Page = require("./page");