const mongoose = require('./mongoose');
const crypto = require('crypto');
const config = require('./db').config;
const schema = require("./Schema/user").schema;

schema.pre("save", function(next){
    const hmac = crypto.createHmac(config.hash, config.secret);
    this.mdp = hmac.update(this.mdp).digest('hex');
    next()
});

schema.methods.compareMDP = function(pass){
    const hmac = crypto.createHmac(config.hash, config.secret);
    const hpass = hmac.update(pass).digest('hex');
    return hpass === this.mdp;
};

module.exports = mongoose.model("User", schema);