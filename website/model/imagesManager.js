const sharp = require('sharp');
const mkdirp = require('mkdirp');
const fs = require("fs");

const imagesDirectory = "./upload/images/";

exports.createPictures = (fileBuffer, fileName) => {
    return new Promise((resolve, reject) => {
        mkdirp(imagesDirectory, err => {
            if(err){
                console.log(err);
                return reject(err);
            }
            else{
                const path = imagesDirectory + fileName;
                let promiseJPEG = sharp(fileBuffer)
                    .jpeg({
                        progressive: true,
                        quality: 60
                    })
                    .toFile(path+".jpeg");
                let promiseWEBP = sharp(fileBuffer)
                    .webp({
                        quality: 60
                    })
                    .toFile(path+".jpeg.webp");
                Promise.all([promiseJPEG, promiseWEBP]).then( () => resolve(path+".jpeg"),
                        err => {
                            console.log(err);
                            reject(err);
                        });
            }
        });
    });
};

exports.deleteImage = (filename) => {
    //delete jpeg
    fs.access(imagesDirectory + filename , err => {
        if(err){
            console.log(err);
        }
        else{
            fs.unlink(imagesDirectory + filename, err => {
                if(err){
                    console.log(err);
                }
            })
        }
    });

    //delete webp
    fs.access(imagesDirectory + filename + ".webp", err => {
        if(err){
            console.log(err);
        }
        else{
            fs.unlink( imagesDirectory + filename + ".webp", err => {
                if(err){
                    console.log(err);
                }
            })
        }
    });
};

module.exports.getImage = (filename, width) => {
    return new Promise((resolve, reject) => {
        fs.access(imagesDirectory + filename , err => {
            if(err){
                console.log(err);
                reject(err);
            }
            else{
                let buffer = sharp(imagesDirectory + filename).resize({
                    width: width,
                    withoutEnlargement: true
                });
                resolve(buffer);
            }
        });
    });
};