**Installation du système en local**

* installer NodeJS et MongoDB (voir plus bas pour plus d'informations)
* restaurer la DB (voir plus bas pour plus d'informations)
* cloner le repo
* dans le dossier /website, utiliser la commande `npm install`
* dans le dossier /website/client, utiliser la commande `npm install`
* dans le projet, il faut changer toutes les adresses qui commencent par `https://allstylesofgaming.com/[...]` par `http://localhost:3000/[...]`. **Normalement**, il ne faut le faire que dans le fichier /website/controleur/upload.js. Mais si des problèmes persistent, c'est qu'une adresse dans un autre fichier n'est pas bonne.
* ouvrir un premier terminal au niveau de /website et effectuer cette commande: `npm run client`
* ouvrir un second terminal au niveau de /website et effectuer cette commande: `npm run server`

==> **Attention**: cette version en local est plus facile à installer mais donne des performances au moins 50% en dessous que la version serveur (production).

**Installation du système pour serveur**

**ATTENTION**: ceci est le guide d'installation sur un **DEBIAN 9** Il est nécessaire d'adapter l'installation à votre config.

* Ajouter répo Nodejs (https://github.com/nodesource/distributions/blob/master/README.md)

`curl -sL https://deb.nodesource.com/setup_12.x | bash -
apt-get install -y nodejs`

* Ajouter répo MongoDB (https://www.digitalocean.com/community/tutorials/how-to-install-mongodb-on-debian-9 et https://docs.mongodb.com/manual/tutorial/install-mongodb-on-debian/)

`apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4`

`echo "deb http://repo.mongodb.org/apt/debian stretch/mongodb-org/4.0 main" | tee /etc/apt/sources.list.d/mongodb-org-4.0.list`

`apt-get update`

`apt-get install -y mongodb-org`

`apt --fix-broken install`

`service mongod start`

* Installer Certbot (https://backports.debian.org/Instructions/  et   https://certbot.eff.org/lets-encrypt/debianstretch-nginx  )

`echo "deb http://deb.debian.org/debian stretch-backports main" | sudo tee /etc/apt/sources.list.d/streatch-backports.list`

`apt-get install certbot python-certbot-nginx -t stretch-backports`

`sudo certbot --nginx certonly`

* Installer Aptitude

`apt-get install -y aptitude `

* Installation Iptables

`apt-get install -y iptables`

* Transférer le script Ipatable sur la machine et le rendre exécutable

`copier/coller le script`

`chmod +x parefeu`

`./parefeu`

* Vérifier si apache installer et le supprimer si c'est le cas

* Installer Nginx (https://nginx.org/en/linux_packages.html#Debian)

`apt install -y curl gnupg2 ca-certificates lsb-release`

``echo "deb http://nginx.org/packages/debian `lsb_release -cs` nginx"  | tee /etc/apt/sources.list.d/nginx.list``

`curl -fsSL https://nginx.org/keys/nginx_signing.key | apt-key add -`

`apt update`

`apt install nginx`

`cd /etc/nginx`

* Copier la config dans nginx.conf et démarrer le service

`nano nginx.conf`

`service nginx start`

* Installer Postfix

`sudo apt install postfix`

* Copier le repo

* Installation des dépendances (**à l'intérieur du dossier avec server.js**) 

`npm install `

* Installation PM2

`npm install -g pm2`

* Restaurer la db

`cd asog`

`cd DB_backup`

`mongorestore`

* Démarrer l'application (dans le dossier avec server.js)

`pm2 start server`

Liste des fichiers:
* Parefeu:
```
#!/bin/sh

#vidage de la table
iptables -F
iptables -X

#changement de la police par défaut
iptables -P INPUT DROP
iptables -P FORWARD DROP
iptables -P OUTPUT DROP

#autorisations pour connexions locales
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

#autorisation connexion SSH
iptables -A INPUT -p tcp --dport 22 -j ACCEPT
iptables -A OUTPUT -p tcp --dport 22 -j ACCEPT

# DNS
iptables -A OUTPUT -p tcp --dport 53 -j ACCEPT
iptables -A OUTPUT -p udp --dport 53 -j ACCEPT
iptables -A INPUT -p tcp --dport 53 -j ACCEPT
iptables -A INPUT -p udp --dport 53 -j ACCEPT


#port ssl
iptables -A INPUT -p tcp --dport 443 -j ACCEPT

#ouverture du port  80/443 pour serveur/wget/curl/etc
iptables -A OUTPUT -p tcp --dport 80 -j ACCEPT
iptables -A OUTPUT -p tcp --dport 443 -j ACCEPT
iptables -A INPUT -p tcp --dport 80 -j ACCEPT
iptables -A INPUT -p tcp --dport 443 -j ACCEPT


# NTP
iptables -A OUTPUT -p udp --dport 123 -j ACCEPT

# SMTP
iptables -A INPUT -p tcp --dport 25 -j ACCEPT
iptables -A OUTPUT -p tcp --dport 25 -j ACCEPT

#autorisations pour les connexions établies
iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -m state --state RELATED,ESTABLISHED -j ACCEPT

```

* Config NGINX:

```
worker_processes auto;


events {
    
}

http{

    server_tokens off;

    #optimisation de l'envoie de fichier
    sendfile on;
    tcp_nopush on;
    tcp_nodelay on;

    #log au minimum pour eviter trop d'ecriture
    access_log off;
    error_log error.log emerg;

    server{
        listen 80 default_server;

        server_name *.allstylesofgaming.com;

        return 301 https://$host$request_uri;
    }

    server{

        listen 443 ssl http2;
        server_name *.allstylesofgaming.com;
        ssl_certificate /etc/letsencrypt/live/www.allstylesofgaming.com/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/www.allstylesofgaming.com/privkey.pem;

        gzip on;
        gzip_disable "msie6";

        gzip_vary on;
        gzip_proxied any;
        gzip_comp_level 6;
        gzip_buffers 16 8k;
        gzip_http_version 1.1;
        gzip_min_length 256;
        gzip_types text/plain text/css application/json application/javascript application/x-javascript text/xml application/xml application/xml+rss text/javascript application/vnd.ms-fontobject application/x-font-ttf font/opentype image/svg+xml image/x-icon;


        
        location *~(/admin){
            proxy_pass http://localhost:8080;
        }

        location ~* \.(?:jpg|jpeg|webp|css|js){
            expires max;
            proxy_pass http://localhost:8080;
        }

        location / {
            proxy_pass http://localhost:8080;
            http2_push /public/image/arcade_scoring.jpg;
            http2_push /public/image/dot.png;
        }
    }
}
```
