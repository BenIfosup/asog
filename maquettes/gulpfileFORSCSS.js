const gulp = require("gulp");
const sass = require("gulp-sass");
//const less = require("gulp-less");
const autoprefixer = require("gulp-autoprefixer");
const plumber = require('gulp-plumber');

gulp.task('cssBuild', () => {
    gulp.src("*.scss")
        .pipe(plumber())
        .pipe(sass({
            outputStyle: "expanded"
        }))
        .pipe(autoprefixer({
            browsers: ['last 4 versions'],
        }))
        .pipe(gulp.dest('.'));
});


gulp.task('default', ['cssBuild'], () => {
    gulp.watch('*.scss', ['cssBuild']);
});